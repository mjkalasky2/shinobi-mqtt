### Shinobi to MQTT bridge
Connecting [Shinobi CCTV](https://shinobi.video/) and your [MQTT](http://mqtt.org/) broker of choice together!

#### Installation

_To update your module version, execute exactly same steps._ 

Open terminal and `cd` into Shinobi home directory:
```bash
cd /home/Shinobi
```
 
Install a new required NPM module:
```bash
npm install mqtt
```
 
Download bridge module to the `libs/customAutoLoad` directory
```bash
wget -O ./libs/customAutoLoad/mqtt.js https://gitlab.com/ovivtash/shinobi-mqtt/raw/master/mqtt.js
```

Restart Shinobi
```bash
pm2 restart camera 
```

#### Configuration

Optionally you can specify different options to the MQTT module by adding a "mqtt" section to Shinobi's `conf.json`.
This can be done either via Superuser UI or by directly editing the file.
Below goes a sample section with default settings and all options explained.

```
{
 ...
 "mqtt": {
    "verbose": false,
    "url": "mqtt://localhost:1883",
    "topic": "shinobi"
 },
 ...
}
```

* _verbose_ - enables a lot more log output, used mainly for debugging
* _url_ - a [URL to your MQTT broker](https://github.com/mqtt/mqtt.github.io/wiki/URI-Scheme) which will receive Shinobi events 
* _topic_ - root MQTT topic where all events will go

#### Message details

Currently only _trigger_ forwarding is supported. Event messages are composed out of Shinobi internal events and gets 
posted to the the topic of the following format:
> <config_prefix>/<user_group_key>/<monitor_id>/<event_type>

Here is an example of message topic and body:
> shinobi/f1VavFNaGn/g0Z7dK6Mfi/trigger
```json
{
  "name": "FULL_FRAME",
  "plugin": "built-in",
  "reason": "motion",
  "confidence": 16,
  "ts": "2019-09-08T19:16:38+03:00"
}
``` 

#### ToDo list
* Implement other handlers like "no motion", monitor start, monitor stop, monitor crash, camera ping failure
* Debouncing option